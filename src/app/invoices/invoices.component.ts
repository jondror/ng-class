import { Component, OnInit } from '@angular/core';
import {InvoicesService} from './invoices.service';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css']
})
export class InvoicesComponent implements OnInit {
  invoices;
  isLoading:boolean=true;

  currentInvoice;

  select(invoice){
    this.currentInvoice = invoice;
  }
  constructor(private _userService:InvoicesService) { }

  ngOnInit() {
    this._userService.getInvoices().subscribe(invoiceData => {
      this.invoices = invoiceData;
      this.isLoading=false;
      });
  }

}
