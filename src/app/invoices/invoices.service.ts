import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2';

@Injectable()
export class InvoicesService {

invoicesObservable;

getInvoices(){
    return this.invoicesObservable = this.af.database.list('/invoices').delay(2000);
  }

addInvoice(invoice){
    this.invoicesObservable = this.af.database.list('/invoices');
    this.invoicesObservable.push(invoice);
  }

  constructor(private af:AngularFire) { }

}
