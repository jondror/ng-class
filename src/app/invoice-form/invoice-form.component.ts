import { Component, OnInit } from '@angular/core';
import {Invoice} from '../invoices/invoice';
import {NgForm} from '@angular/forms';
import {InvoicesService} from '../invoices/invoices.service';

@Component({
  selector: 'app-invoice-form',
  templateUrl: './invoice-form.component.html',
  styleUrls: ['./invoice-form.component.css']
})
export class InvoiceFormComponent implements OnInit {
  invoice:Invoice = {pname:'', amount:'1000'};
  noProduct:boolean=false;
  noAmount:boolean=false;
  formData:NgForm;

  checkInput(form:NgForm){
      (this.invoice.pname == "") ? this.noProduct=true :  this.noProduct=false;
      (this.invoice.amount == "") ? this.noAmount=true :  this.noAmount=false;
      if(!this.noAmount && !this.noProduct){
        this.formData=form;
        this.submitForm(this.formData);
      }
   
    }
    submitForm(form:NgForm){
      console.log("The Product Name is:"+this.invoice.pname+" And the amount is: "+this.invoice.amount);
      this._invoicesService.addInvoice(this.invoice);
      this.invoice = {
        pname:'',
        amount:'1000'
        }
    }

  constructor(private _invoicesService:InvoicesService) { }

  ngOnInit() {
  }

}
