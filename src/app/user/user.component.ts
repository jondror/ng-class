import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {User} from './user';

@Component({
  selector: 'jce-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs: ['user']
})
export class UserComponent implements OnInit {
  user:User;
  @Output() deleteEvent = new EventEmitter<User>();
  @Output() editEvent = new EventEmitter<User>();

  isEdit:Boolean=false;

  editButtonText:string="Edit";

  constructor() { } 

  sendDelete(){
    this.deleteEvent.emit(this.user);
    //create a delete event and emit (send) the user data.
  }
  toggleEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText="Update" : this.editButtonText="Edit";
    if(!this.isEdit){ //clicked on "Save", the button is now changed back to "Edit" and we save the data
      this.editEvent.emit(this.user);
    }

  }

  ngOnInit() {
  }

}
