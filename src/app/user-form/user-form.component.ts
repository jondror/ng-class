import {NgForm} from '@angular/forms';
import {User} from '../user/user';
import {Component, OnInit, Output, EventEmitter} from '@angular/core';



@Component({
  selector: 'jce-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  user:User = {name:'', email:''};
  @Output() userAddedEvent = new EventEmitter<User>();
  constructor() { }

  

  onSubmit(form:NgForm){
    console.log(form);
    this.userAddedEvent.emit(this.user);
    this.user = {
      name:'',
      email:''
    }

  }
  ngOnInit() {
  }

}
